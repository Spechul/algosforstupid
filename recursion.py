def req_bracket(n: int):
    if n <= 0: # guard clause
        return

    print('{', end='') # recursion down
    req_bracket(n - 1)
    print('}', end='') # recursion up


def gcd(a: int, b: int) -> int:
    if b == 0:
        return a
    else:
        return gcd(b, a % b)


def all_reshuffles():
    pass


def generate_numbers(base: int, length: int, prefix=None):
    """base-ary count system < 10"""
    prefix = prefix or []
    if length == 0:
        print(prefix)
        return

    for digit in range(base):
        prefix.append(digit)
        generate_numbers(base, length - 1, prefix)
        prefix.pop()


def generate_bin(length, prefix=''):
    if length == 0:
        print(prefix)
    else:
        generate_bin(length-1, prefix+'0')
        generate_bin(length-1, prefix+'1')

if __name__ == '__main__':
    #generate_numbers(5, 4)
    generate_bin(4)
    pass