from numpy import inf, isinf, zeros
import inspect
import pprint
import copy


def task1():
    print(inspect.stack()[0][3])
    g = [
      [0, 9, inf, 3, inf, inf ,inf ,inf],
      [9, 0, 2, inf, 7, inf, inf, inf],
      [inf, 2, 0, 2, 4, 8, 6, inf],
      [3, inf, 2, 0, inf, inf, 5, inf],
      [inf, 7, 4, inf, 0, 10, inf, inf],
      [inf, inf, 8, inf, 10, 0, 7, inf],
      [inf, inf, 6, 5, inf, 7, 0, inf],
      [inf, inf, inf, inf, 9, 12, 10, 0]
    ]
    return g

def task2():
    print(inspect.stack()[0][3])
    g = [
      [0, 3, 2, 6, inf, inf ,inf ,inf, inf],
      [inf, 0, inf, 2, inf, inf, inf ,inf ,inf],
      [inf, inf ,0, inf ,inf, 4, inf, inf ,inf],
      [inf, inf, 3, 0, 1, inf, 6, inf, inf],
      [inf, inf, inf, inf, 0, inf, 7, 5, inf],
      [inf, inf, inf, inf, 5, 0, inf, 4, inf],
      [inf, inf, inf, inf, inf, inf, 0, 2, 4],
      [inf, inf, inf, inf, inf, inf, inf, 0, 4],
      [inf, inf, inf, inf, inf, inf, inf, inf, 0]
    ]
    return g



def task3():
    print(inspect.stack()[0][3])
    g = [
      [0, 3, 2, 6, inf, inf ,inf ,inf, inf],
      [inf, 0, inf, 2, inf, inf, inf ,inf ,inf],
      [inf, inf , 0, inf ,inf, 4, inf, inf ,inf],
      [inf, inf, 3, 0, 1, inf, 6, inf, inf],
      [inf, inf, inf, inf, 0, inf, 7, 5, inf],
      [inf, inf, inf, inf, 5, 0, inf, 4, inf],
      [inf, inf, inf, inf, inf, inf, 0, 2, 4],
      [inf, inf, inf, inf, inf, inf, inf, 0, 15],
      [inf, inf, inf, inf, inf, inf, inf, inf, 0]
    ]
    return g


def task4():
    print(inspect.stack()[0][3])
    g = [
      [0, 3, 4, inf, 5, inf ,inf,inf, ],
      [inf, 0, 2, 1, inf, inf, 4, inf],
      [inf, inf, 0, 3, 2, inf, inf, inf],
      [inf, inf, inf, 0, inf, inf, 3, inf],
      [inf, inf, inf, 4, 0, 8, inf, 3],
      [inf, inf, inf, 5, inf, 0, inf, 2],
      [inf, inf, inf, inf, inf, 2, 0, 1],
      [inf, inf, inf, inf, inf, inf, inf, 0],
    ]
    return g


def task5():
    print(inspect.stack()[0][3])
    g = [
      [0, 3, 4, inf, 5, inf, inf, inf],
      [inf, 0, inf, 1, inf, inf, 4, inf],
      [inf, inf, 0, 3, 2, inf, inf, inf],
      [inf, inf, inf, 0, inf, inf, 1, inf],
      [inf, inf, inf, 4, 0, 8, inf, 3],
      [inf, inf, inf, 5, inf, 0, inf, 2],
      [inf, inf, inf, inf, inf, 2, 0, 1],
      [inf, inf, inf, inf, inf, inf, inf, 0],
    ]
    return g


def task6():
    print(inspect.stack()[0][3])
    g = [
      [0, 6, 1, 5, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, 0, 4, inf, 2, 3, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, 0, 2, inf, inf, 5, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, inf, 0, inf, inf, 6, 6, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, inf, inf, 0, inf, inf, inf, 10, inf, inf, inf, inf, inf, inf],
      [inf, inf, inf, inf, 4, 0, inf, inf, inf, 7, inf, inf, inf, inf, inf],
      [inf, inf, inf, inf, inf, 20, 0, inf, inf, 10, 5, inf, inf, inf, inf],
      [inf, inf, inf, inf, inf, inf, 2, 0, inf, inf, 3, 4, inf, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, 0, 1, inf, inf, 3, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, 0, inf, inf, 2, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, 8, 0, inf, inf, 2, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, 0, inf, 1, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, 0, 3, 4],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, 0, 5],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, 0],
    ]
    return g


def task7():
    print(inspect.stack()[0][3])
    g = [
      [0, 2, inf, inf, 5, inf, inf, 7, inf, inf, 3, inf, inf, inf],
      [inf, 0, 3, inf, 4, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, 0, 7, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, inf, 0, inf, inf, 9, inf, inf, inf, inf, inf, inf, 3],
      [inf, inf, 1, inf, 0, 2, inf, inf, inf, inf, inf, inf, inf, inf],
      [inf, inf, 5, 8, inf, 0, inf, inf, 2, 3, inf, inf, inf, inf],
      [inf, inf, inf, inf, inf, inf, 0, inf, inf, inf, inf, inf, inf, 5],
      [inf, inf, inf, inf, inf, 1, inf, 0, inf, inf, 4, 2, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, 0, inf, inf, 7, 1, inf],
      [inf, inf, inf, inf, inf, inf, 4, inf, inf, 0, inf, inf, inf, 2],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, 0, 6, inf, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, 0, 4, inf],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, 0, 4],
      [inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, inf, 0],
    ]
    return g


class FloydWarshall:
    def __init__(self, g):
        self.g = copy.deepcopy(g)
        self.n = len(g[0])
        self.p = [list(range(self.n)) for _ in range(self.n)]#[[-inf]*self.n for _ in range(self.n)]#[list(range(1, self.n+ 1)) for _ in range(self.n)]

    def solve(self):
        for k in range(self.n):
          for i in range(self.n):
            for j in range(self.n):
                if self.g[i][j] > self.g[i][k] + self.g[k][j]:
                    self.p[i][j] = k
                    self.g[i][j] = self.g[i][k] + self.g[k][j]

        return self


if __name__=='__main__':
    g = task1()
    fw = FloydWarshall(g)
    fw.solve()
    print('g:')
    pprint.pprint(fw.g)
    print('path:')
    pprint.pprint(fw.p)
