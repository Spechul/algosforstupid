import math


def eratosphen_sieve(N: int) -> list:
    A = [True] * (N + 1)
    sqrt_n = math.sqrt(N + 1)
    for i in range(2, int(sqrt_n) + 1):
        if A[i]:
            for j in range(i**2, N + 1, i):
                A[j] = False
    return A


if __name__ == '__main__':
    a = eratosphen_sieve(1000000000) # 1000000000 -> ~8,34 gb of ram
    with open('primes.txt', 'w') as file:
        for i in range(len(a)):
            if a[i]:
                file.write(str(i) + '\n')