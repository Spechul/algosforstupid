def fact(n: int) -> int:
    if n < 2:
        return 1

    return n * fact((n-1))


def fact_no_rec(n: int) -> int:
    f = n
    while n > 1:
        n -= 1
        f *= n
    return f


class Node2:
    def __init__(self, value: int):
        self.left = None
        self.right = None
        self.value = value

    def __str__(self):
        return self.value.__str__()

import typing


def tree_straight_lookup(root: Node2, operation: typing.Callable):
    operation(root)
    if root.left:
        tree_straight_lookup(root.left, operation)
    if root.right:
        tree_straight_lookup(root.right, operation)


def tree_straight_lookup_no_rec(root: Node2, operation: typing.Callable):
    nodes = []
    nodes_copy = []
    nodes.append(root)
    nodes_copy.append(root)
    current_node = root
    while True:
        if current_node.left and current_node.left not in nodes:
            nodes.append(current_node.left)
            nodes_copy.append(current_node.left)
            current_node = current_node.left

        elif current_node.right and current_node.right not in nodes:
            nodes.append(current_node.right)
            nodes_copy.append(current_node.right)
            current_node = current_node.right

        if not nodes_copy:
            break

        if current_node.left is None and current_node.right is None:
            current_node = nodes_copy.pop()

    nodes.reverse()
    while nodes:
        current_node = nodes.pop()
        operation(current_node)

if __name__ == '__main__':
    #ls = [1,2]
    #print(ls.pop())

    #print(fact_no_rec(5))
    n = Node2(1)
    n.left = Node2(2)
    n.left.left = Node2(3)
    n.right = Node2(4)
    n.left.right = Node2(5)
    #tree_straight_lookup(n, lambda x: print(x.value))
    tree_straight_lookup_no_rec(n, lambda x: print(x.value))