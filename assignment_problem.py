from numpy import inf, isinf
from ford_fulkerson import FordFulkersonSolver
import inspect


class AssignmentProblemSolver:
    def __init__(self, c):
        self.c = c
        self.n = len(c[0])

    def change_rows(self):
        for i in range(self.n):
            m = min(self.c[i])
            if isinf(m):
                raise ValueError("The restriction are not compatible")
            self.c[i] = [self.c[i][j] - m for j in range(self.n)]

    def change_columns(self):
        for i in range(self.n):
            m = min(self.c[j][i] for j in range(self.n))
            if isinf(m):
                raise ValueError("The restriction are not compatible")
            for j in range(self.n):
                self.c[j][i] -= m

    def change_c(self):
        self.change_rows()
        self.change_columns()

    def create_u1(self):
        for i in range(1, self.n + 1):
            self.net.append([0, i, 1])

    def create_u_ast(self):
        for i in range(self.n + 1, 2 * self.n + 1):
            self.net.append([i, 2 * self.n + 1, 1])

    def create_u0(self):
        for i in range(self.n):
            for j in range(self.n):
                if self.c[i][j] == 0:
                    self.net.append([i + 1, j + 1 + self.n, inf])

    def create_net(self):
        self.net = []
        self.create_u1()
        self.create_u_ast()
        self.create_u0()

    def create_answer(self):
        self.ans = [0 for _ in range(self.n)]
        for i, j, c, x in self.max_flow.edges:
            if i in range(1, self.n + 1) and j in range(self.n + 1, 2 * self.n + 1) and x == 1:
                self.ans[i - 1] = j - self.n - 1
        return self.ans

    def create_N(self):
        self.N1 = set([])
        self.N2 = set([])
        for index in self.max_flow.i_ast:
            i, j, c, x = self.max_flow.edges[index]
            for var in [i, j]:
                if var == 0:
                    continue
                if var > self.n:
                    self.N2.add(var - self.n - 1)
                else:
                    self.N1.add(var - 1)

    def calculate_alpha(self):
        self.alpha = inf
        for i in self.N1:
            for j in range(self.n):
                if j not in self.N2 and self.c[i][j] < self.alpha:
                    self.alpha = self.c[i][j]

    def add_alpha_to_c(self):
        for i in range(self.n):
            for j in range(self.n):
                if i in self.N1 and j not in self.N2:
                    self.c[i][j] -= self.alpha
                elif i not in self.N1 and j in self.N2:
                    self.c[i][j] += self.alpha

    def solve(self):
        self.change_c()
        while True:
            self.create_net()
            s, t, size = 0, 2 * self.n + 1, 2 * self.n + 2
            self.max_flow = FordFulkersonSolver(self.net, size, s, t).solve()
            if self.max_flow.flow == self.n:
                return self.create_answer()
            self.create_N()
            self.calculate_alpha()
            self.add_alpha_to_c()


def task1():
    print(inspect.stack()[0][3])
    c = [
        [6, 4, 13, 4, 19, 15, 11, 8],
        [17, 15, 18, 14, 0, 7, 18, 7],
        [3, 5, 11, 9, 7, 7, 18, 16],
        [17, 10, 16, 19, 9, 6, 1, 5],
        [14, 2, 10, 14, 11, 6, 4, 10],
        [17, 11, 17, 12, 1, 10, 6, 19],
        [13, 1, 4, 2, 2, 7, 2, 14],
        [12, 15, 19, 11, 13, 1, 7, 8]
    ]
    return c


def task2():
    print(inspect.stack()[0][3])
    c = [
        [9, 6, 4, 9, 3, 8, 0],
        [5, 8, 6, 8, 8, 3, 5],
        [5, 2, 1, 1, 8, 6, 8],
        [1, 0, 9, 2, 5, 9, 2],
        [9, 2, 3, 3, 0, 3, 0],
        [7, 3, 0, 9, 4, 5, 6],
        [0, 9, 6, 0, 8, 8, 9]
    ]
    return c


def task3():
    print(inspect.stack()[0][3])
    c = [
        [6, 6, 2, 4, 7, 1, 9, 4, 6],
        [5, 0, 2, 4, 9, 2, 9, 2, 0],
        [7, 6, 0, 5, 2, 3, 0, 5, 5],
        [9, 5, 8, 9, 2, 3, 1, 5, 7],
        [3, 1, 7, 3, 0, 2, 2, 8, 1],
        [3, 0, 0, 6, 1, 7, 2, 4, 7],
        [5, 6, 1, 9, 9, 8, 4, 1, 8],
        [5, 4, 5, 2, 2, 6, 6, 5, 6],
        [3, 6, 1, 6, 3, 0, 5, 2, 2]
    ]
    return c


def task4():
    print(inspect.stack()[0][3])
    c = [
        [6, 5, 6, 8, 4, 0, 4, 6],
        [5, 7, 8, 7, 4, 4, 0, 9],
        [0, 7, 9, 2, 8, 7, 0, 3],
        [6, 6, 6, 3, 0, 3, 0, 8],
        [7, 4, 7, 1, 1, 1, 8, 9],
        [8, 0, 7, 5, 0, 9, 1, 3],
        [3, 2, 4, 7, 1, 7, 3, 4],
        [9, 2, 4, 3, 2, 4, 3, 9]
    ]
    return c


def task5():
    print(inspect.stack()[0][3])
    c = [
        [7, 4, 5, 3, 8, 9, 6, 5, 5, 3, 2],
        [5, 6, 9, 4, 9, 0, 0, 4, 4, 7, 2],
        [8, 8, 3, 2, 7, 3, 7, 6, 7, 4, 6],
        [7, 4, 9, 9, 3, 7, 3, 8, 1, 5, 8],
        [5, 2, 4, 3, 3, 9, 6, 2, 5, 1, 3],
        [9, 4, 5, 8, 6, 3, 3, 1, 7, 6, 5],
        [9, 1, 0, 3, 1, 2, 7, 6, 9, 4, 6],
        [5, 6, 8, 0, 9, 9, 1, 9, 3, 0, 8],
        [4, 6, 5, 6, 4, 7, 5, 3, 8, 0, 1],
        [2, 3, 7, 8, 4, 9, 5, 0, 2, 8, 0],
        [7, 6, 7, 1, 9, 5, 7, 4, 2, 3, 0]
    ]
    return c


def task6():
    print(inspect.stack()[0][3])
    c = [
        [7, -4, 5, 3, 8, 9, 6, 5],
        [5, 6, 9, 4, 9, 0, 0, 4],
        [8, 8, 3, -2, 7, -3, 7, 6],
        [7, 4, 9, 9, 3, 7, 3, 8],
        [5, 2, 4, 3, 3, 9, 6, 2],
        [9, 4, 5, 8, 6, 3, 3, 1],
        [9, 1, 0, -3, 1, 2, 7, 6],
        [5, 6, 8, 0, 9, 9, 1, 9]
    ]
    return c


def task7():
    print(inspect.stack()[0][3])
    c = [
        [2, 6, 5, -1, 6, 1, 8, 4, 6],
        [2, 1, 2, 7, 9, -2, 8, 2, 0],
        [0, 6, 0, 5, 1, 3, 4, 3, 5],
        [7, 0, 8, 9, 2, 4, 1, 6, 7],
        [-1, 1, 0, -3, 0, 2, 2, 2, 1],
        [3, 0, 6, 6, 1, -2, 2, 4, 0],
        [1, 7, 1, 9, 4, 8, 2, 6, 8],
        [5, 1, 5, 2, 2, 6, -1, 5, 4],
        [3, 6, 0, 6, 3, 0, 9, 1, 2]
    ]
    return c


def task8():
    print(inspect.stack()[0][3])
    c = [
      [2, 4, 0, 3, 8, -1, 6, 5],
      [8, 6, 3, 4, 2, 0, 0, 4],
      [8, -4, 3, 2, 7, 3, 1, 0],
      [2, 4, 9, 5, 3, 0, 3, 8],
      [5, 2, 7, 3, -1, 0, 3, 2],
      [3, 2, 5, 1, 5, 3, 0, 1],
      [2, 1, 0, -3, 1, 2, 7, 0],
      [1, 6, 4, 0, 0, 9, 1, 7]
    ]
    return c


if __name__ == '__main__':
    c = task8()
    aps = AssignmentProblemSolver(c)
    print(aps.solve())
    # print(aps.ans)
