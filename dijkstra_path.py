def task():
    start = 1
    finish = 5
    n = 6
    lines = [
        [1, 2, 7],
        [1, 3, 9],
        [1, 6, 14],
        [2, 3, 10],
        [2, 4, 15],
        [3, 4, 11],
        [3, 6, 2],
        [4, 5, 6],
        [5, 6, 9]
    ]
    #print(G)
    return lines, start, finish, n


def task1():
    start = 1
    n = 10
    lines = [
        [1, 2, 5],
        [1, 8, 3],
        [2, 7, 3],
        [2, 3, 2],
        [3, 5, 5],
        [4, 3, 2],
        [5, 4, 1],
        [5, 10, 2],
        [6, 3, 4],
        [6, 5, 1],
        [6, 8, 6],
        [7, 3, 2],
        [7, 6, 5],
        [7, 1, 2],
        [8, 7, 4],
        [8, 2, 1],
        [8, 9, 1],
        [9, 10, 5],
        [10, 4, 6],
        [10, 6, 3],
    ]
    return lines, start, n


def task2():
    start = 1
    n = 9
    lines = [
        [1, 2, 6],
        [1, 3, 2],
        [1, 7, 2],
        [2, 3, 5],
        [2, 6, 6],
        [3, 6, 1],
        [4, 5, 3],
        [4, 3, 2],
        [6, 1, 4],
        [6, 5, 6],
        [6, 5, 1],
        [6, 7, 3],
        [6, 8, 7],
        [7, 8, 4],
        [8, 2, 1],
        [8, 9, 1],
        [9, 4, 1],
        [9, 5, 5],
        [9, 6, 2],
    ]
    return lines, start, n


def task3():
    start = 1
    n = 8
    lines = [
        [1, 2, 3],
        [1, 7, 4],
        [2, 3, 4],
        [2, 6, 8],
        [2, 8, 6],
        [3, 5, 6],
        [4, 3, 2],
        [5, 2, 2],
        [5, 7, 2],
        [6, 3, 5],
        [6, 4, 1],
        [6, 1, 2],
        [6, 8, 2],
        [6, 5, 4],
        [7, 6, 6],
        [7, 4, 1],
        [8, 1, 1],
        [8, 7, 5],
    ]
    return lines, start, n


def task4():
    start = 1
    n = 8
    lines = [
        [1, 2, 4],
        [1, 3, 3],
        [1, 6, 7],
        [1, 7, 2],
        [1, 8, 1],
        [2, 8, 1],
        [2, 3, 5],
        [3, 4, 2],
        [4, 6, 3],
        [4, 5, 1],
        [6, 5, 6],
        [7, 5, 7],
        [7, 6, 2],
        [7, 3, 4],
        [8, 7, 5],
        [8, 4, 6],
        [8, 3, 4],
    ]
    return lines, start, n


def task5():
    start = 1
    n = 7
    lines = [
        [1, 2, 4],
        [1, 6, 6],
        [2, 3, 7],
        [2, 4, 1],
        [2, 6, 3],
        [3, 4, 2],
        [3, 5, 5],
        [3, 4, 2],
        [4, 1, 2],
        [4, 6, 4],
        [4, 5, 6],
        [5, 7, 1],
        [6, 5, 2],
        [6, 7, 3],
        [7, 2, 7],
        [7, 3, 1],
        [7, 4, 3],
    ]
    return lines, start, n


def task6():
    start = 1
    n = 8
    lines = [
        [1, 3, 4],
        [1, 6, 1],
        [1, 8, 4],
        [2, 1, 9],
        [2, 3, 1],
        [3, 4, 4],
        [3, 5, 2],
        [3, 6, 5],
        [5, 4, 3],
        [5, 8, 8],
        [6, 2, 2],
        [6, 5, 7],
        [6, 8, 3],
        [7, 2, 10],
        [7, 8, 3],
        [8, 4, 6],
    ]
    return lines, start, n


def task7():
    start = 1
    n = 9
    lines = [
        [1, 2, 5],
        [1, 7, 4],
        [1, 8, 3],
        [2, 3, 6],
        [2, 7, 11],
        [3, 4, 3],
        [3, 5, 1],
        [3, 6, 5],
        [3, 9, 6],
        [4, 8, 7],
        [5, 4, 5],
        [6, 1, 6],
        [6, 2, 2],
        [6, 5, 5],
        [7, 3, 2],
        [7, 8, 7],
        [8, 6, 4],
        [8, 5, 3],
        [8, 9, 4]
    ]
    return lines, start, n


def task8():
    start = 1
    n = 8
    lines = [
        [1, 2, 1],
        [1, 6, 5],
        [1, 7, 6],
        [1, 8, 5],
        [2, 3, 4],
        [2, 4, 4],
        [2, 5, 6],
        [3, 4, 5],
        [4, 8, 8],
        [5, 4, 3],
        [5, 8, 1],
        [5, 7, 9],
        [6, 5, 4],
        [6, 7, 3],
        [7, 2, 2],
    ]
    return lines, start, n


def dijkstra(g, start, length):
    d = [float('inf') for _ in range(length + 1)]
    used = [False for i in range(length + 1)]
    d[start] = 0
    p = [0]*(length + 1)
    for i in range(length + 1):
        v = -1
        for j in range(length + 1):
            if not used[j] and (v == -1 or d[j] < d[v]):
                v = j
        if d[v] == float('inf'):
            break
        used[v] = True
        for j in range(len(g[v])):
            to = g[v][j][0]
            leng = g[v][j][1]

            if d[v] + leng < d[to]:
                d[to] = d[v] + leng
                p[to] = v

    #print(p)
    return d


def main():
    lines, start, n = task8()

    g = [[] for _ in range(n + 1)]

    g[0].append([0, 0])
    for l in lines:
        g[l[0]].append([l[1], l[2]])



    d = dijkstra(g, start, n)
    print(d)
    pass


if __name__ == '__main__':
    main()
