from numpy import inf
import inspect


def task1():
    print(inspect.stack()[0][3])
    inp = [
        [0, 1, 4],
        [0, 3, 9],
        [1, 3, 2],
        [1, 4, 4],
        [2, 4, 1],
        [2, 5, 10],
        [3, 2, 1],
        [3, 5, 6],
        [4, 5, 1],
        [4, 6, 2],
        [5, 6, 9]
    ]
    s, t, n = 0, 6, 7
    return inp, n, s, t

def task2():
    print(inspect.stack()[0][3])
    inp = [
      [0, 1, 3],
      [0, 2, 2],
      [0, 3, 1],
      [0, 5, 6],
      [1, 3, 1],
      [1, 4, 2],
      [2, 3, 1],
      [2, 4, 2],
      [2, 5, 4],
      [3, 4, 7],
      [3, 6, 4],
      [3, 7, 1],
      [3, 5, 5],
      [4, 6, 3],
      [4, 7, 2],
      [5, 7, 4],
      [6, 7, 5],
      [6, 5, 3]
    ]
    s, t, n = 0, 7, 8
    return inp, n, s, t


def task3():
    print(inspect.stack()[0][3])
    inp = [
      [0, 1, 4],
      [0, 6, 2],
      [0, 5, 5],
      [0, 4, 1],
      [0, 2, 1],
      [1, 3, 1],
      [1, 5, 6],
      [2, 4, 2],
      [3, 6, 3],
      [3, 2, 6],
      [4, 7, 3],
      [4, 5, 4],
      [5, 6, 1],
      [5, 7, 3],
      [5, 8, 6],
      [6, 8, 5],
      [6, 7, 4],
      [7, 8, 4]
    ]
    s, t, n = 0, 8, 9
    return inp, n, s, t


def task4():
    print(inspect.stack()[0][3])
    inp = [
      [0, 1, 3],
      [0, 2, 6],
      [0, 3, 3],
      [0, 4, 2],
      [1, 2, 4],
      [1, 3, 1],
      [1, 4, 4],
      [2, 7, 2],
      [2, 6, 3],
      [3, 2, 1],
      [3, 6, 1],
      [3, 7, 2],
      [4, 5, 1],
      [5, 6, 1],
      [5, 3, 3],
      [6, 7, 4],
    ]
    s, t, n = 0, 7, 8
    return inp, n, s, t


def task5():
    print(inspect.stack()[0][3])
    inp = [
      [0, 1, 2],
      [0, 3, 5],
      [0, 5, 3],
      [1, 3, 1],
      [2, 1, 4],
      [2, 8, 1],
      [2, 4, 1],
      [2, 7, 3],
      [3, 2, 2],
      [3, 4, 2],
      [3, 6, 5],
      [3, 5, 4],
      [4, 1, 4],
      [4, 5, 4],
      [4, 8, 5],
      [6, 4, 1],
      [6, 7, 4],
      [7, 4, 3],
      [8, 7, 2]
    ]
    s, t, n = 0, 8, 9
    return inp, n, s, t


def task6():
    print(inspect.stack()[0][3])
    inp = [
      [0, 3, 1],
      [0, 4, 6],
      [1, 0, 1],
      [1, 3, 4],
      [1, 4, 7],
      [2, 0, 5],
      [2, 1, 5],
      [3, 2, 1],
      [3, 7, 3],
      [3, 6, 1],
      [3, 4, 2],
      [4, 5, 4],
      [4, 6, 3],
      [4, 5, 4],
      [5, 2, 5],
      [5, 7, 2],
      [6, 5, 7],
    ]
    s, t, n = 0, 7, 8
    return inp, n, s, t


def task7():
    print(inspect.stack()[0][3])
    inp = [
      [0, 4, 4],
      [0, 6, 2],
      [0, 5, 6],
      [1, 0, 3],
      [1, 4, 7],
      [1, 2, 2],
      [1, 6, 4],
      [3, 1, 3],
      [3, 2, 5],
      [3, 8, 2],
      [3, 4, 2],
      [4, 5, 1],
      [5, 8, 7],
      [6, 5, 1],
      [7, 3, 6],
      [7, 2, 3],
      [7, 8, 1],
      [7, 6, 4]
    ]
    s, t, n = 0, 8, 9
    return inp, n, s, t


def task8():
    print(inspect.stack()[0][3])
    inp = [
      [0, 1, 3],
      [0, 2, 3],
      [0, 5, 2],
      [0, 6, 1],
      [1, 2, 6],
      [2, 3, 1],
      [2, 4, 2],
      [2, 6, 7],
      [2, 5, 4],
      [3, 4, 7],
      [3, 9, 4],
      [3, 4, 7],
      [4, 9, 1],
      [4, 8, 1],
      [4, 6, 1],
      [5, 4, 7],
      [5, 3, 4],
      [6, 9, 3],
      [7, 1, 1],
      [7, 0, 2],
      [7, 5, 5],
      [7, 6, 6],
      [7, 8, 2],
      [8, 9, 5]
    ]
    s, t, n = 0, 9, 10
    return inp, n, s, t


class FordFulkersonSolver:
    def add_values(self, i, j, c, x):
        self.g[i].append(len(self.edges))
        self.edges.append([i, j, c, x])

    def create_graph(self, inp):
        for i, j, c in inp:
          self.add_values(i, j, c, 0)
          self.add_values(j, i, 0, 0)

    def __init__(self, inp, n, s, t):
        self.n = n
        self.g = [[] for _ in range(n)]
        self.edges = []
        self.create_graph(inp)
        self.s = s
        self.t = t
        self.flow = 0

    def dfs(self, v):
        for index in self.g[v]:
            if self.used[self.t]:
                return
            i, j, c, x = self.edges[index]
            if not self.used[j] and c - x > 0:
                self.used[j] = True
                self.edges_in_increasing_path.append(index)
                self.i_ast.append(index)
                self.dfs(j)
                if self.used[self.t]:
                    return
                self.edges_in_increasing_path.pop()
                self.used[j] = False

    def set_min_cut(self):
        self.min_cut = inf
        for index in self.edges_in_increasing_path:
            i, j, c, x = self.edges[index]
            self.min_cut = min(self.min_cut, c - x)
        self.flow += self.min_cut

    def create_increasing_path(self):
        self.used = [False for _ in range(self.n)]
        self.edges_in_increasing_path = []
        self.i_ast = []
        self.used[0] = True
        self.dfs(0)

    def change_flow(self):
        for index in self.edges_in_increasing_path:
            self.edges[index][3] += self.min_cut
            self.edges[index^1][3] -= self.min_cut

    def solve(self):
        while True:
            self.create_increasing_path()
            if not self.used[self.t]:
                return self
            self.set_min_cut()
            self.change_flow()


if __name__ == '__main__':
    vals = task8()
    ffs = FordFulkersonSolver(*vals)
    print(ffs.solve().flow)