from numpy import *
from simplex_method import SimplexMethodSolver
from functools import reduce
import inspect

class DualSimplexMethodSolver:

    def __init__(self, A, b, c, d_low, d_high, basic_indexes = None, nonbasic_indexes = None):
        self.A = A
        self.b = b
        self.c = c
        self.d_low = d_low
        self.d_high = d_high
        self.m, self.n = A.shape
        self.basic_indexes = basic_indexes
        self.nonbasic_indexes = nonbasic_indexes
        self.eps = 0.0000001

    def is_zero(self, val):
        return abs(val) < self.eps

    def init_indexes(self):
        if self.basic_indexes is None:
            plan = SimplexMethodSolver(self.A, self.b, self.c).first_phase()
            self.A = plan.A
            self.b = plan.b
            self.c = plan.c
            self.m = plan.m
            self.n = plan.n
            self.basic_indexes = plan.basic_indexes
            self.nonbasic_indexes = plan.nonbasic_indexes
            #print(self.basic_indexes)

    def init_auxiliary_variables(self):
        self.basic_A = self.A[:, self.basic_indexes]
        self.B = linalg.inv(self.basic_A)
        self.y = dot(self.c[self.basic_indexes], self.B)
        self.delta = subtract(dot(self.y, self.A), self.c)
        #print(self.y)
        #print(self.delta)

    def init_non_basic_indexes(self):
        self.nonbasic_indexes_plus = set([i for i in self.nonbasic_indexes if self.delta[i] > 0 or self.is_zero(self.delta[i])])
        self.nonbasic_indexes_minus = set(self.nonbasic_indexes) - self.nonbasic_indexes_plus

    def init_necessary_params(self):
        self.init_indexes()
        self.init_auxiliary_variables()
        self.init_non_basic_indexes()

    def create_ksi_for_nonbasic_indexes(self):
        for i in range(self.n):
            if i in self.nonbasic_indexes_minus:
                self.ksi.append(self.d_high[i])
            elif i in self.nonbasic_indexes_plus:
                self.ksi.append(self.d_low[i])
            else:
                self.ksi.append(0)

    def create_ksi_for_basic_indexes(self):
        s = reduce(lambda x, y: add(x, y), [dot(self.A[:,j], self.ksi[j]) for j in self.nonbasic_indexes])
        ksi_basic = dot(self.B, subtract(self.b, s))

        for i, index in enumerate(self.basic_indexes):
            self.ksi[index] =  ksi_basic[i]

    def create_ksi(self):
        self.ksi = []
        self.create_ksi_for_nonbasic_indexes()
        self.create_ksi_for_basic_indexes()

    def get_index(self, a, el):
        try:
            return list(a).index(el)
        except ValueError:
            return -1

    def calculate_mu(self):
        self.mjk = 1.0 if self.ksi[self.jk] < self.d_low[self.jk] else -1.0
        dy = dot(dot(self.mjk, eye(self.m)[:, self.k]), self.B)
        self.mu = [dot(dy, self.A[:, j]) for j in self.nonbasic_indexes]

    def calculate_sigma0(self):
        def condition(i, j):
            return (j in self.nonbasic_indexes_plus and (self.mu[i] < 0 and not self.is_zero(self.mu[i])) or (j in self.nonbasic_indexes_minus and self.mu[i] > 0 and not self.is_zero(self.mu[i])))
        sigma = [-self.delta[j]/self.mu[i] if condition(i, j) else inf for i, j in enumerate(self.nonbasic_indexes)]
        self.sigma0 = min(sigma)
        self.asterix = sigma.index(self.sigma0)
        self.j_ast = self.nonbasic_indexes[self.asterix]

    def change_delta(self):
        for i, j in enumerate(self.nonbasic_indexes):
            self.delta[j] += self.sigma0*self.mu[i]
        self.delta[self.jk] += self.sigma0*self.mjk

    def change_B(self):
        z = dot(self.B, self.A[:, self.j_ast])
        zk = z[self.k]
        z[self.k] = -1
        z /= -zk
        M = eye(self.m)
        M[:, self.k] = z
        self.B = dot(M, self.B)

    def correct_basic_indexes(self):
        self.basic_indexes[self.k] = self.j_ast
        self.basic_A[:, self.k] = self.A[:, self.j_ast]
        self.change_B()

    def correct_nonbasic_indexes(self):
        self.nonbasic_indexes[self.asterix] = self.jk
        if self.mjk == 1:
            if self.j_ast in self.nonbasic_indexes_plus:
                self.nonbasic_indexes_plus.remove(self.j_ast)
                self.nonbasic_indexes_plus.add(self.jk)
            else:
              self.nonbasic_indexes_plus.add(self.jk)
        else:
            if self.j_ast in self.nonbasic_indexes_plus:
                self.nonbasic_indexes_plus.remove(self.j_ast)
        self.nonbasic_indexes_minus = set(self.nonbasic_indexes) - self.nonbasic_indexes_plus

    def solve(self):
        self.init_necessary_params()
        while(True):
            self.create_ksi()
            #print(self.ksi)
            self.jk = self.get_index(logical_and(self.ksi >= self.d_low, self.ksi <= self.d_high), False)
            if not ~self.jk:
                self.x = self.ksi
                return self
            self.k = self.basic_indexes.index(self.jk)
            self.calculate_mu()
            self.calculate_sigma0()
            if isinf(self.sigma0):
                raise ValueError("This task hasn't available plans!")
            self.change_delta()
            self.correct_basic_indexes()
            self.correct_nonbasic_indexes()


def test():
    A = array([
        array([2, -1, 1, 0, 0, -1, 3], dtype=float),
        array([0, 4, -1, 2, 3, -2, 2], dtype=float),
        array([3, 1, 0, 1, 0, 1, 4], dtype=float)
    ])
    b = array([1.5, 9, 2], dtype=float)
    c = array([0, 1, 2, 1, -3, 4, 7], dtype=float)
    d_low = array([0, 0, -3, 0, -1, 1, 0], dtype=float)
    d_high = array([3, 3, 4, 7, 5, 3, 2], dtype=float)

    return A, b, c, d_low, d_high


def task1():
    print(inspect.stack()[0][3])
    A = array([
        array([1, -5, 3, 1, 0, 0], dtype=float),
        array([4, -1, 1, 0, 1, 0], dtype=float),
        array([2, 4, 2, 0, 0, 1], dtype=float)
    ])
    b = array([-7, 22, 30], dtype=float)
    c = array([7, -2, 6, 0, 5, 2], dtype=float)
    d_low = array([2, 1, 0, 0, 1, 1], dtype=float)
    d_high = array([6, 6, 5, 2, 4, 6], dtype=float)

    return A, b, c, d_low, d_high


def task2():
    print(inspect.stack()[0][3])
    A = array([
        array([1, 0, 2, 2, -3, 3], dtype=float),
        array([0, 1, 0, -1, 0, 1], dtype=float),
        array([1, 0, 1, 3, 2, 1], dtype=float)
    ])
    b = array([15, 0, 13], dtype=float)
    c = array([3, 0.5, 4, 4, 1, 5], dtype=float)
    d_low = array([0, 0, 0, 0, 0, 0], dtype=float)
    d_high = array([3, 5, 4, 3, 3, 4], dtype=float)

    return A, b, c, d_low, d_high


def task3():
    print(inspect.stack()[0][3])
    A = array([
        array([1, 0, 0, 12, 1, -3, 4, -1], dtype=float),
        array([0, 1, 0, 11, 12, 3, 5, 3], dtype=float),
        array([0, 0, 1, 1, 0, 22, -2, 1], dtype=float)
    ])
    b = array([40, 107, 61], dtype=float)
    c = array([2, 1, -2, -1, 4, -5, 5, 5], dtype=float)
    d_low = array([0, 0, 0, 0, 0, 0, 0, 0], dtype=float)
    d_high = array([3, 5, 5, 3, 4, 5, 6, 3], dtype=float)

    return A, b, c, d_low, d_high


def task4():
    print(inspect.stack()[0][3])
    A = array([
        array([1, -3, 2, 0, 1, -1, 4, -1, 0], dtype=float),
        array([1, -1, 6, 1, 0, -2, 2, 2, 0], dtype=float),
        array([2, 2, -1, 1, 0, -3, 8, -1, 1], dtype=float),
        array([4, 1, 0, 0, 1, -1, 0, -1, 1], dtype=float),
        array([1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=float)
    ])
    b = array([3, 9, 9, 5, 9], dtype=float)
    c = array([-1, 5, -2, 4, 3, 1, 2, 8, 3], dtype=float)
    d_low = array([0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=float)
    d_high = array([5, 5, 5, 5, 5, 5, 5, 5, 5], dtype=float)

    return A, b, c, d_low, d_high


def task5():
    print(inspect.stack()[0][3])
    A = array([
        array([1, 7, 2, 0, 1, -1, 4], dtype=float),
        array([0, 5, 6, 1, 0, -3, -2], dtype=float),
        array([3, 2, 2, 1, 1, 1, 5], dtype=float)
    ])
    b = array([1, 4, 7], dtype=float)
    c = array([1, 2, 1, -3, 3, 1, 0], dtype=float)
    d_low = array([-1, 1, -2, 0, 1, 2, 4], dtype=float)
    d_high = array([3, 2, 2, 5, 3, 4, 5], dtype=float)

    return A, b, c, d_low, d_high


def task6():
    print(inspect.stack()[0][3])
    A = array([
        array([2, -1, 1, 0, 0, -1, 3], dtype=float),
        array([0, 4, -1, 2, 3, -2, 2], dtype=float),
        array([3, 1, 0, 1, 0, 1, 4], dtype=float)
    ])
    b = array([1.5, 9, 2], dtype=float)
    c = array([0, 1, 2, 1, -3, 4, 7], dtype=float)
    d_low = array([0, 0, -3, 0, -1, 1, 0], dtype=float)
    d_high = array([3, 3, 4, 7, 5, 3, 2], dtype=float)

    return A, b, c, d_low, d_high


def task7():
    print(inspect.stack()[0][3])
    A = array([
        array([2, 1, 0, 3, -1, -1], dtype=float),
        array([0, 1, -2, 1, 0, 3], dtype=float),
        array([3, 0, 1, 1, 1, 1], dtype=float)
    ])
    b = array([2, 2, 5], dtype=float)
    c = array([0, -1, 1, 0, 4, 3], dtype=float)
    d_low = array([2, 0, -1, -3, 2, 1], dtype=float)
    d_high = array([7, 3, 2, 3, 4, 5], dtype=float)

    return A, b, c, d_low, d_high


def task8():
    print(inspect.stack()[0][3])
    A = array([
        array([1, 3, 1, -1, 0, -3, 2, 1], dtype=float),
        array([2, 1, 3, -1, 1, 4, 1, 1], dtype=float),
        array([-1, 0, 2, -2, 2, 1, 1, 1], dtype=float)
    ])
    b = array([4, 12, 4], dtype=float)
    c = array([2, -1, 2, 3, -2, 3, 4, 1], dtype=float)
    d_low = array([-1, -1, -1, -1, -1, -1, -1, -1], dtype=float)
    d_high = array([2, 3, 1, 4, 3, 2, 4, 4], dtype=float)

    return A, b, c, d_low, d_high


if __name__ == '__main__':
    vals = task8()
    c = vals[2]
    x0 = DualSimplexMethodSolver(*vals).solve()
    print('x0 =', x0.x)
    print("c'x0 = ", dot(c, x0.x))

