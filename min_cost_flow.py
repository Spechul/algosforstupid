from numpy import *
import inspect


def swap(a, b):
    temp = a[:]
    a = b[:]
    b = temp[:]
    return a, b


def make_basis_graph(n, S):
    g = dict()
    for i in range(n):
        g[i] = []

    for i in range(n):
        for j in range(len(S)):
            if S[j][4] == 1:
                if i == S[j][0]:
                    g[i].append(S[j][1])
                    g[S[j][1]].append(i)

    return g


def dfs_u(v, g, S, u, used):
    used[v] = True
    for to in g[v]:
        if used[to] == False:
            for elem in S:
                if v == elem[0] and to == elem[1]:
                    u[to] = u[v] - elem[2]
                elif v == elem[1] and to == elem[0]:
                    u[to] = u[v] + elem[2]
            dfs_u(to, g, S, u, used)


def solve_potential(n, g, S):
    used = [False for i in range(n)]
    u = [inf for i in range(n)]
    u[0] = 0
    dfs_u(0, g, S, u, used)
    return u


def solve_marks(S, u):
    delta = []
    for elem in S:
        if elem[4] == 0:
            delta.append(((elem[0], elem[1]), u[elem[0]] - u[elem[1]] - elem[2]))

    return delta


class MatrixNetTaskSolver:
    def __init__(self, cycle_st=0, cycle_end=0):
        self.cycle_st = cycle_st
        self.cycle_end = cycle_end

    def dfs(self, v, color, p, g):
        color[v] = 1
        for to in g[v]:
            if color[to] == 0:
                p[to] = v
                if (self.dfs(to, color, p, g)):
                    return True

            elif (color[to] == 1) and (p[v] != to):
                self.cycle_end = v
                self.cycle_st = to
                return True

        color[v] = 2
        return False

    def cycle(self, uu, n, g):
        color = []
        p = []
        _cycle = []
        for i in range(n):
            color.append(0)
            p.append(-1)

        if self.dfs(uu, color, p, g):

            v = self.cycle_end
            while (v != self.cycle_st):
                _cycle.append(v)
                v = p[v]

            _cycle.append(self.cycle_st)

        return _cycle

    def solve(self, n, S):
        while True:
            g1 = make_basis_graph(n, S)
            u = solve_potential(n, g1, S)
            delta = solve_marks(S, u)
            temp_list = [elem[1] for elem in delta]
            max_delta = max(temp_list)
            if max_delta <= 0:
                sum = 0
                for elem in S:
                    sum += elem[2] * elem[3]

                return sum, S # [[elem[0], elem[1], elem[3]] for elem in S ]

            ind = temp_list.index(max_delta)
            curve_0 = delta[ind][0]
            for i in range(len(S)):
                if S[i][0] == curve_0[0] and S[i][1] == curve_0[1]:
                    S[i][4] = 1

            g2 = make_basis_graph(n, S)
            U = self.cycle(curve_0[0], n, g2)
            U.reverse()
            U.append(U[0])
            U_plus = []
            U_minus = []
            for i in range(len(U) - 1):
                for elem in S:
                    if U[i] == elem[0] and U[i + 1] == elem[1]:
                        U_plus.append((U[i], U[i + 1]))
                    elif U[i] == elem[1] and U[i + 1] == elem[0]:
                        U_minus.append((U[i + 1], U[i]))

            if curve_0 not in U_plus:
                U_minus, U_plus = swap(U_minus, U_plus)

            theta = []
            for elem in S:
                tupl = (elem[0], elem[1])
                if tupl in U_minus:
                    theta.append((tupl, elem[3]))

            theta0 = min([theta[i][1] for i in range(len(theta))])
            for elem in theta:
                if elem[1] == theta0:
                    curve_star = elem[0]
                    break

            for curve in U_plus:
                for i in range(len(S)):
                    if curve[0] == S[i][0] and curve[1] == S[i][1]:
                        S[i][3] += theta0
                        break

            for curve in U_minus:
                for i in range(len(S)):
                    if curve[0] == S[i][0] and curve[1] == S[i][1]:
                        S[i][3] -= theta0
                        break

            for i in range(len(S)):
                if curve_star[0] == S[i][0] and curve_star[1] == S[i][1]:
                    S[i][4] = 0
                    break


def test_task():
    S = [
        [0, 1, 1, 1, 1],
        [1, 5, 3, 0, 0],
        [2, 1, 3, 3, 1],
        [2, 3, 5, 1, 1],
        [4, 2, 4, 0, 0],
        [4, 3, 1, 5, 1],
        [5, 2, 3, 9, 1],
        [5, 4, 4, 0, 0],
        [5, 0, -2, 0, 0]
    ]
    n = 6
    return n, S

'''from, to, c, x, basic'''
def task1():
    print(inspect.stack()[0][3])
    S = [
        [0, 1, 9, 2, 1],
        [0, 7, 5, 7, 1],
        [1, 2, 1, 4, 1],
        [1, 5, 3, 0, 0],
        [1, 6, 5, 3, 1],
        [2, 8, -2, 0, 0],
        [3, 2, -3, 0, 0],
        [4, 3, 6, 3, 1],
        [5, 4, 8, 4, 1],
        [6, 2, -1, 0, 0],
        [6, 3, 4, 0, 0],
        [6, 4, 7, 5, 1],
        [6, 8, 1, 0, 0],
        [7, 6, 2, 0, 0],
        [7, 8, 2, 0, 0],
    ]
    n = 10
    return n, S


def task2():
    print(inspect.stack()[0][3])
    S = [
        [0, 1, 8, 5, 1],
        [0, 7, 3, 0, 1],
        [1, 2, 2, 0, 0],
        [1, 6, 9, 0, 0],
        [2, 5, 4, 0, 0],
        [3, 2, -2, 0, 0],
        [3, 5, 1, 0, 0],
        [4, 3, 8, 6, 1],
        [5, 4, 4, 0, 0],
        [6, 2, 11, 1, 1],
        [6, 4, 6, 7, 1],
        [6, 5, 2, 0, 0],
        [7, 5, 5, 6, 1],
        [7, 6, 5, 5, 1]
    ]
    n = 8
    return n, S


def task3():
    print(inspect.stack()[0][3])
    S = [
        [0, 1, 8, 5, 1],
        [1, 7, 3, 4, 1],
        [1, 6, 9, 3, 1],
        [1, 2, 2, 0, 0],
        [2, 5, 4, 0, 0],
        [3, 2, -2, 0, 0],
        [4, 3, -3, 6, 1],
        [5, 4, 8, 7, 1],
        [6, 2, 13, 4, 1],
        [6, 3, 1, 0, 0],
        [6, 4, 1, 0, 0],
        [6, 5, 7, 3, 1],
        [7, 6, 1, 0, 0],
        [7, 5, -1, 0, 0],
    ]
    n = 8
    return n, S


def task4():
    print(inspect.stack()[0][3])
    S = [
        [0, 1, 1, 0, 0],
        [0, 6, 4, 3, 1],
        [1, 2, 5, 2, 1],
        [1, 4, 3, 0, 0],
        [2, 3, 3, 0, 0],
        [2, 5, 2, 0, 0],
        [4, 3, 10, 4, 1],
        [4, 3, 5, 7, 1],
        [4, 5, 2, 0, 0],
        [5, 3, 1, 0, 0],
        [6, 4, 8, 2, 1],
        [6, 5, 6, 5, 1],
    ]
    n = 7
    return n, S


def task5():
    print(inspect.stack()[0][3])
    S = [
        [0, 1, 3, 0, 0],
        [0, 4, 5, 2, 1],
        [0, 5, 4, 4, 1],
        [0, 6, 2, 0, 0],
        [1, 2, 5, 3, 1],
        [1, 4, -1, 0, 0],
        [1, 6, 7, 1, 1],
        [2, 3, 6, 2, 1],
        [2, 6, 1, 0, 0],
        [3, 5, 1, 0, 0],
        [4, 2, 2, 0, 0],
        [4, 3, -2, 0, 0],
        [4, 5, 1, 0, 0],
        [5, 6, 7, 5, 1],
    ]
    n = 8
    return n, S


def task6():
    print(inspect.stack()[0][3])
    S = [
        [0, 1, 6, 2, 1],
        [0, 5, 2, 0, 0],
        [0, 6, -2, 0, 0],
        [1, 2, 3, 0, 0],
        [1, 3, 6, 2, 1],
        [1, 5, 1, 0, 0],
        [2, 4, 3, 0, 0],
        [2, 5, 4, 6, 1],
        [3, 2, -1, 0, 0],
        [3, 7, 1, 0, 0],
        [4, 7, 7, 5, 1],
        [5, 4, 5, 3, 1],
        [5, 7, 3, 0, 0],
        [5, 6, 5, 3, 1],
        [6, 4, 2, 0, 0],
        [6, 7, 2, 0, 0],
        [6, 1, 4, 4, 1]
    ]
    n = 8
    return n, S


def task7():
    print(inspect.stack()[0][3])
    S = [
        [0, 1, 7, 2, 1],
        [0, 2, 6, 3, 1],
        [2, 3, 6, 4, 1],
        [2, 4, 5, 4, 1],
        [5, 6, 4, 2, 1],
        [6, 4, 7, 5, 1],
        [1, 2, 4, 0, 0],
        [1, 5, 3, 0, 0],
        [3, 5, 1, 0, 0],
        [4, 3, 4, 0, 0],
        [4, 5, -1, 0, 0],
        [0, 4, 3, 0, 0],
        [6, 0, 2, 0, 0]
    ]
    n = 7
    return n, S


def task8():
    print(inspect.stack()[0][3])
    S = [
        [0, 1, 5, 4, 1],
        [0, 5, 5, 2, 1],
        [1, 3, 10, 5, 1],
        [2, 3, 6, 1, 1],
        [5, 4, 4, 3, 1],
        [2, 1, 3, 0, 0],
        [2, 0, 1, 0, 0],
        [2, 4, 2, 0, 0],
        [1, 5, 3, 0, 0],
        [0, 4, 1, 0, 0],
        [3, 4, 3, 0, 0],
        [5, 3, 2, 0, 0]
    ]
    n = 7
    return n, S


if __name__ == "__main__":
    mtts = MatrixNetTaskSolver()
    sum, res = mtts.solve(*task6())#(n, S)
    print(sum)
    #print(res)
    for arr in res:
      print(arr)