def move(_from, to):
    print("move ", _from, "to", to, "!")

def hanoi(number, _from, through, to):
    if number == 0: return
    #print(number)
    hanoi(number-1, _from, to, through)
    move(_from, to)
    hanoi(number-1, through, _from, to)

if __name__ == '__main__':
    hanoi(10, "A", "B", "C")
