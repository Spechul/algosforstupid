import pprint


def task1():
    c = 6
    n = 3
    lines = [
        [0, 1, 2, 2, 4, 5, 6], # f1
        [0, 2, 3, 5, 7, 7, 8], # f2
        [0, 2, 4, 5, 6, 7, 7], # f3
    ]

    return lines, c + 1, n


def task2():
    c = 6
    n = 3
    lines = [
        [0, 1, 1, 3, 6, 10, 11], # f1
        [0, 2, 3, 5, 6, 7, 13], # f2
        [0, 1, 4, 4, 7, 8, 9], # f3
    ]

    return lines, c + 1, n

def task3():
    c = 7
    n = 3
    lines = [
        [0, 1, 2, 4, 8, 9, 9, 23], # f1
        [0, 2, 4, 6, 6, 8, 10, 11], # f2
        [0, 3, 4, 7, 7, 8, 8, 24], # f3
    ]

    return lines, c + 1, n

def task4():
    c = 7
    n = 3
    lines = [
        [0, 3, 3, 6, 7, 8, 9, 14], # f1
        [0, 2, 4, 4, 5, 6, 8, 13], # f2
        [0, 1, 1, 2, 3, 3, 10, 11], # f3
    ]

    return lines, c + 1, n


def task5():
    c = 8
    n = 4
    lines = [
        [0, 2, 2, 3, 5, 8, 8, 10, 17], # f1
        [0, 1, 2, 5, 8, 10, 11, 13, 15], # f2
        [0, 4, 4, 5, 6, 7, 13, 14, 14], # f3
    ]

    return lines, c + 1, n

def task6():
    c = 11
    n = 4
    lines = [
        [0, 1, 3, 4, 5, 8, 9, 9, 11, 12, 12, 14],  # f1
        [0, 1, 2, 3, 3, 3, 7, 12, 13, 14, 17, 19],  # f2
        [0, 4, 4, 7, 7, 8, 12, 14, 14, 16, 18, 22],  # f3
        [0, 5, 5, 5, 7, 9, 13, 13, 15, 15, 19, 24],  # f4
    ]

    return lines, c + 1, n


def task7():
    c = 11
    n = 5
    lines = [
        [0, 4, 4, 6, 9, 12, 12, 15, 16, 19, 19, 19],  # f1
        [0, 1, 1, 1, 4, 7, 8, 8, 13, 13, 19, 20],  # f2
        [0, 2, 5, 6, 7, 8, 9, 11, 11, 13, 13, 18],  # f3
        [0, 1, 2, 4, 5, 7, 8, 8, 9, 9, 15, 19],  # f4
        [0, 2, 5, 7, 8, 9, 10, 10, 11, 14, 17, 21],  # f5
    ]

    return lines, c + 1, n


def task8():
    c = 10
    n = 6
    lines = [
        [0, 1, 2, 2, 2, 3, 5, 8, 9, 13, 14],  # f1
        [0, 1, 3, 4, 5, 5, 7, 7, 10, 12, 12],  # f2
        [0, 2, 2, 3, 4, 6, 6, 8, 9, 11, 17],  # f3
        [0, 1, 1, 1, 2, 3, 9, 9, 11, 12, 15],  # f4
        [0, 2, 7, 7, 7, 9, 9, 10, 11, 12, 13],  # f5
        [0, 2, 5, 5, 5, 6, 6, 7, 12, 18, 22],  # f6
    ]

    return lines, c + 1, n

def bellman(f, index, z, y, B):
    #print(index, z)
    a_1 = f[index][z]
    a_2 = B[index - 1][y - z][0]
    return a_1 + a_2, z if index > 0 else 0
    pass


def solve(f, c, n):
    B = [[tuple([0, 0])]*(c) for i in range(n)]
    #print(B)
    z = 1
    for i in range(c):
        B[0][i] = (f[0][i], 0)
    #print(B)

    for i in range(n):
        for j in range(c):
            z = 0
            ls = []
            z_s = [i in range(1, j + 1)]
            while z <= j:
                ls.append(bellman(f, i, z, j, B))
                z += 1
            #print(ls)
            B[i][j] = max(ls, key=lambda x: x[0])

    x_s = []
    cur_x = B[-1][-1][1]
    i = len(B) - 1
    j = len(B[0]) - 1

    while i > 0:
        x_s.append(B[i][j])
        j -= cur_x
        i -= 1
        cur_x = B[i][j][1]

    pprint.pprint(B, width=200 if len(B[0]) > 7 else 80)

    print(x_s)
    #B = max(f[index][z] + bellman(f, y, new_z))


if __name__ == '__main__':
    t = task7()
    solve(*t)